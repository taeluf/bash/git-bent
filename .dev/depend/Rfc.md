# RFC: A custom external dependency system for git
Depending on a limited number of files from an external repo can often bring a lot of baggage like tests and documentation. Sometimes, I just want to copy a single file from a repo directly into my repo. I don't want a git submodule or a composer require. I want a local copy of the code. But, it would be nice to run an automated `update` on it if I so choose. 

One example would be the left_pad_str "library" from npm. It was one function many packages relied upon, which caused great issues when the single function was removed from npm. A local copy would resolve that issue.


