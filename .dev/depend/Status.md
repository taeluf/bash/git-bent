# Status
`bent depend` feature idea currently abandoned. The development time required to polish this feature is not worthwhile for me (Reed), personally.


# Depend on arbitrary files from another repo 

0. start on source branch (your own library, which depends upon a different repo)
1. new branch (`depend_branch`) to point to external repo
2. delete all files (except .git folder)
3. commit
4. switch back to source branch
5. git merge depend_branch -s ours
6. switch to `depend_branch`
7. git pull https://depend_url.com/whatever.git target_branch_name --allow-unrelated-histories
    - may want a nocommit flag (but probably not)
8. switch to `source_branch`
9. `git merge depend_branch`
10. switch to depend_branch
11. wait for changes on the target
12. Run the git pull https://def.com/abc.git command above
13. Should be up to date
14. switch to source branch
15. `git merge depend_branch`
16. Edit one of the files that is depended upon

17. Idk yet 
    - I think `git merge -s ours` keeps the non-existed files from merging, but it also stops the modified file from merging
    - I think I just need to git merge -s --no-commit, delete every file that is added (I think I have an internal function that'll list all modified files & should be easy to filter from there). Then commit
    - Not 100% sure what this will do for the merge back in


This whole workflow....


It might be better to simply git clone the target repo into a local cache directory
Then copy+paste files from there into my depend branch
But this will create problems when fast-forwarding & trying to keep local changes & remote changes in-sync



Ok... so...

I have one branch that is an exact copy of my target
I have a 2nd branch which is filtered to only have the files I need from the target
Then I have the source branch with the library code

The source branch merges into/from the filtered depend branch
The filtered depend branch merges into & from the pure branch


The only thing I don't like about all of this is... Adding extra branches is clutter & I don't like clutter!!!

So I could make the depend process a local-only thing... but that would undoubtedly cause issues. Merge conflicts at some distant point down the line.
