# RFC: General Purpose installer for Git
To simplify installing software across various programming languages, a general purpose package installer could be written. Git is widely used on various platforms. Other version control systems may be viable as well, but the early stage focus is git. Installing a repo would include downloading it, moving it to the correct location, and making it available to the cli of the system. An early version would focus on bash cli. The installer repo would contain unofficial installer scripts for various repos. Repos could also write and contain their own installer scripts in a location of their choice (only 2 or 3 options would be allowed). 

Git Bent would be a good candidate for directly integrating this installer. The repo could get very large if many installer scripts are written. 

A simple version could:
1. Download a repo
2. append to `~/.bashrc` an `alias` or an `export PATH` or declare a `function`

A more complex version could:
1. Download a repo
2. Check if dependent programs are installed
3. Install missing programs (via dnf, apt, or another existing package manager)
4. Run operations on the repo (like `make` or `yarn install` or `composer install`)


A more complex multi-package integration would be...
Downloading a php repository directly through git to make available to the whole system. This would include adding it as a cli program (if applicable) & adding it to a global autoloader. The global autoloading would be handled by a different package. Extensibility would allow the autoloader package to add an integration to receive calls from installers. The installers would tell the php autoloader package where new php packages were located. The php autoloader package would then handle the rest.



Important Features:
- Choose the install location when downloading a package.
    - Default: `~/.bent/packge_name/` or `~/.bent/package/package_name/`
        - `~/bent/gitlab`, `~/bent/github`, `~/bent/mycustomgithosting`, etc might be cool
    - `~/.package_name`
    - `~/apps/package_name/` (or `~/dev/` or another)
    - What other install paths are common?
    - How would it work on windows? (I think its the same if using git bash or WSL)
    - What about global install? (Not supported in early version? Personally, I'd rather not use `sudo` access if I don't have to.)
- Update all packages from a single command
- Delete / remove packages
- Use existing directory instead of downloading (but still link to git url)
- Depend upon branch of choice
- Depend upon release of choice (if tags are used)
- Depend upon range of releases (`1.0.*` to get `1.0.0` and `1.1.8` & prefer the higher number. Composer(php)'s syntax would probably work)
    - This may be too complex to implement in an early version. I think it's an important feature, but it's not the most important. A simpler(?) alternative: Display the list of tags & let the `bent`er choose which one to install / update to


Extra Features that would be super cool:
- I'm tired.
