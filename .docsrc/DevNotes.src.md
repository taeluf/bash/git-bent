# General Notes
Just a bunch of stuff I might want to use at some point in time, maybe.

## Ideas for Git Bent
- bent tips: Tips on how to use the command line, and Git Bent
    - maybe a `bent first time` command? That's what `setup` is supposed to be, but maybe a different name?
- bent reset: Reset local files to match the git host
    - Maybe `bent sync` This could have multiple options:
        - Overwrite local changes with what's on the git host
        - Overwrite remote files with what's here locally
        - stash local changes, pull remote & overwrite, then merge local changes onto remote by hand
- bent switch project: 
- bent switch user:
- bent switch feature:
    - This might just be part of bent switch version, unless there is some branch filtering
- bent install gitlab.com/User/Pakage.git
    - A general purpose installer for git repos. edits bashrc with path, or something simple like that. Maybe run additional build commands like 'make'.
    - JSDELIVR may be helpful with this, as a means to download code from git hosts.

# Marketing Opportunities
- https://stackoverflow.com/questions/57220224/git-push-with-ssh 
    - Specifically the ssh setup feature that walks you through & generates ssh key for you
- https://stackoverflow.com/questions/3065650/whats-the-simplest-way-to-list-conflicted-files-in-git
    - Post our solution for finding files with conflicts

## Unlikely ideas for Git Bent
- bent issues / feature request / bug report / feedback
- bent fork (go to a repo online & fork it)
- bent pull request (go online)

## Bash Resources
- https://stackoverflow.com/questions/64079876/should-i-compile-my-bash-library-into-fewer-files?noredirect=1#comment113314739_64079876
    - A thread of very helpful comments on designing this library better (even though that's not what the question was about lol)
- https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

## Git Resources
- https://github.com/newren/git-filter-repo/ Quickly rewrite git repository history (filter-branch replacement) 
- https://github.com/GitTools/GitVersion Versioning when using git, solved. GitVersion looks at your git history and works out the semantic version of the commit being built.
- https://github.com/GitTools/GitReleaseManager Tool for creating and exporting releases for software applications hosted on GitHub 
- https://github.com/GitTools/GitReleaseNotes Utility which makes it really easy to generate release notes for your Git project. Works with GitHub, Jira and YouTrack. TFS Support coming soon
- https://git-scm.com/docs/gitk
- tig: https://github.com/jonas/tig
- git bfg: https://rtyley.github.io/bfg-repo-cleaner/
    - An improvement on git filter-branch
- Git bisect: http://webchick.net/node/99
- https://github.com/grdl/git-get
- https://github.com/jwiegley/git-scripts
- https://github.com/bill-auger/git-branch-status/
- Push local repo to production server: https://help.dreamhost.com/hc/en-us/articles/216445197-Pushing-your-local-Git-repository-to-a-DreamHost-server-Linux-Mac-OS-X 

## Other Resources
- 

## Non-Git Commands
- `ssh-keygen -l -f private_key_file` mixed with `ssh-add -l` can help me figure out if an ssh identity has already been added
- `ssh -o BatchMode=yes git@github.com` check if I have a successful ssh connection to github
    - `ssh -i /c/Users/bwimbush/.ssh/id_rsa -v git@remoteserver.com 'git --version'` provides a more verbose way of looking at it

## Git Commands
- `git rm --cached password.txt`: Remove a single file... from tracking? Does this work only work on a file that is newly tracked, or does it work on all files that have long been committed? I think it removes the file from the current commit
- `git rm --cached -rf a-dir`: Remove a directory
- `git ls-files -o # show untracked files`
- `git ls-files -m # show tracked files with unstaged changes`
- `git reset --hard $(git rev-list --max-parents=0 HEAD)`
- `git clean -fdx` will clean even ignored files & directories... for deleting cached files??
- `git add --renormalize` which fixes all newlines, but first do `echo "* text=auto" >> .gitattributes`
- `git sneak` = commit --amend --no-edit
- `git add -p` (review changes before adding them). `git checkout . -p` (review changes before removing them)
- `git log --all --pretty=oneline --decorate --graph --abbrev-commit`: A much better log
    - https://coderwall.com/p/euwpig/a-better-git-log
    - `git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit`
    - add `-p` to see lines that changed
    - credit to @filipekiss on Twitter
- `git rebase -i`... not sure what it does
    - `git rebase -i HEAD~2`: If you only want to alter the last two commits (great for using fixup if the last commit was juast a fix of the commit before)
    - "In this case you can also use `git commit --amend` it will add current index to last commit. You can use `--no-edit` to use prev commit message"
- `git filter-branch`
- `git reset --hard commit_hash; git push -f origin [branch]`
- `git bisect`
- `git pull origin master --rebase --autostash`
    1. Stash local changes
    2. Pulls remote so local dir is now equal to remote
    3. Merges the stash, overwriting the latest remote with those local changes.
    4. May need to resolve conflicts if any exist.
- `git checkout -b branch_name specific-commit` to restore a branch that has been deleted
    - I'm guessing you can checkout any commit as a branch this way?
- `git shortlog -s -n`
    - Shows all contributors of a git project ordered by the number of commits
- Merge a pull request from the same repo (non-fork)
    1. Step 1: From your project repository, bring in the changes and test.
    ```bash
    git fetch origin
    git checkout -b feature-dynamic_urls origin/feature-dynamic_urls
    git merge version_1
    ```
    2. Step 2: Merge the changes and update on GitHub.
    ```bash
    git checkout version_1
    git merge --no-ff feature-dynamic_urls
    git push origin version_1
    ```
- Replace master branch with what's currently on disk:
    ```bash
    git checkout -b a_temporary_branch 
    git merge -s ours --no-commit master
    git commit      # Add a message regarding the replacement that you just did
    git checkout master
    git merge a_temporary_branch
    git push
    #then probably delete the temporary branch? Maybe archive it? Not sure
    ```
