# TODOs
These may also be scattered throughout the codebase... Search `@TODO` across the codebase for more things TODO

## unsorted
- [bent find] find project, find file (in current state or across commit history?), find string in files 
- [bent cd project_name] or [bent switch project PROJECT_NAME] to cd into project dir
    - by printing `cd /path/to/project`, which you must then copy+paste into your terminal
- [bent check] to: check current status, download latest updates, and whatever else I think of. Intention is to run at the start of your code day to get your project into a proper state


## delete
- [bent delete project]: Delete project online
- [bent delete submodule]: Remove a submodule from your project
- [bent delete file]: Remove a file from git, but maybe not from local machine? Idk

## config
- Configuration options. 
    - to auto-upload on save or not
    - multiple remotes, multiple users
    - Colors of headers, instructional messages, status messages, etc

## howto/help
- setting up webhooks
- Setting up git-based web deployment

## other
- [bent archive]: create a tag, prefixed with archive/
- [bent reset]: force local to be identical to remote files
- [bent reset remote]: Force remote to be identical to local files
- [bent publish]: push to a webserver

## internals
- write tests
- Create a `prompt_ordefault` function. Example output: `We will upload to 'origin': ([enter]-confirm / c-cancel / e-edit): `
    - This could also take a string input & use that over the default instead of having an `e-edit` step



