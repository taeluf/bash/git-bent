# screenshots
Sorry, I haven't added good alt text. Maybe someone else can contribute that?

`bent help`   
![bent help](/.image/bent-help.png)

`bent help core`   
![bent help core](/.image/bent-help-core.png)

`bent diff`  
![bent diff](/.image/bent-diff.png)

`bent revert` (will make a new commit reverting to the previous commit) 
![bent revert](/.image/bent-revert.png)

`bent status`  
![bent status](/.image/bent-status.png)
