<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# Git Bent  
Cli library to do git stuff, but better. It has guided prompts, simple versions of complex commands, a nice `git log` output, built-in ability to roll back commits, and so much more.  
  
You may want to look at the [`Dev Notes`](/doc/DevNotes.md) for some additional resources & things about git.  
  
## Screenshots  
For more see [screenshots dir](/.image/).  
  
this is `bent diff`, to view the diff between files:  
![bent diff command, showing a list of different files to switch between with option to enter a number or range of numbers to select the files you view git diffs for. Then there's a little bit of the diff of a selected file](/.image/bent-diff.png)  
  
  
## Status: Beta! & pretty good  
Be a tester!   
  
I've been using this for over a year, and I'm pretty happy with this version. I trust it to work for me. Why is it still in Beta? Because i need testers to confirm it works for others & maybe find bugs & test compatibility on Windows & Mac.   
  
It's also ... messy. There will likely be some breaking changes. It needs polish. Help menus work, but they're ... not as intuitive as I'd like. Some desired features are missing.  
  
## Getting Started  
We'll `git clone` this package and run the install script, which will download our single dependency & setup `~/.bashrc` to point to this library.  
1. `cd ~/; mkdir vendor; cd vendor;` or `cd` into another directory for downloaded packages  
2. `git clone https://gitlab.com/taeluf/bash/git-bent.git git-bent`  
3. `cd git-bent; ./install;` - install downloads my bash cli library it depends on & appends itself to your `~/.bashrc` file  
4. Either `source ~/.bashrc` or re-launch your terminal  
  
## Connect  
- [@TaelufDev](https://twitter.com/TaelufDev) on Twitter   
- [taeluf.com](https://taeluf.com) - My site is not currently maintained (Feb 3, 2021). My site is still not being maintained (September 18, 2021)... Wow. :(  My poor poor website  
  
## Contribute  
This project is somewhat actively developed. Please open issues & pull requests as you see fit. Better yet, write some bash. If you've never used bash before, then [DevHints.io/bash](https://devhints.io/bash) is awesome. If you know how to code & you've used linux cli, you should be able to jump right in and make small fixes or add small features.  
  
Please ask questions (on twitter, preferably) if you want to contribute & would like guidance or have questions. If you do something "wrong", don't worry - I'll change it to my liking.  
  
## Alternatives / Competition  
We make no guarantees regarding software listed here.   
- [Git Scripts collection](https://github.com/jwiegley/git-scripts) by [jwiegley](https://github.com/jwiegley)  
- [Gitkraken](https://www.gitkraken.com/), a GUI which is free for open source projects, by [@GitKraken](https://twitter.com/gitkraken)  
- [Vershd](https://vershd.io/) "The effortless Git GUI for Windows, Mac, and Linux." by [@Blightysoft](https://twitter.com/blightysoft)  
  
### Why use Git Bent over competition?  
- You like CLI   
- I've worked really hard on it & just want to make git easier & more useful  
- I still want to make it better, but have no reason to if others don't use it.  
  
### Why NOT use Git Bent?  
- You have VERY specific git needs (you can use both though)  
- You don't like that our `save` function does `add -A` every time  
- You love GUIs and hate CLI and hate being happy  
- You don't trust me & you think it's full of viruses because I'm an internet rando.  
  
## Development  
If you want to contribute, you do not need php. I will gladly run the documentation generation myself & eventually will do this with CICD.   
  
This uses PHP packages to generate documentation & help menus. This requires [composer](https://getcomposer.org/) (to install dependencies), [php language](https://www.php.net), and I'm pretty sure composer requires [git](https://git-scm.com/), but you probably have that already  
  
To setup the dev environment, make a fork, then:  
```  
git clone git@gitlab.com:your_vendor_name/bash/git-bent.git git-bent # Put in the correct url for your fork.  
cd git-bent/.config  
composer install  
cd ..  
```  
Then to generate documentation & help menus:  
```  
.config/vendor/taeluf/code-scrawl/cli/scrawl  
```  
& just answer `'y'`  
  
