# Status

## October 18th, 2021
I added a `test-git-hooks` branch to see what a hooks-setup feature would need to do.

## July 7, 2021
- Write `bent depend` feature 
    - The initial depending is fully implemented, but may need polish & may have bugs
    - The updating is laid out in comments
    - See `Status/BentDepend.md`

## Next
- Release after scrawl & lexer are updated & docs + help menus regenerated

## High Priority Features & Bugs 

## Low Priority Features
- See .docsrc/Todo.src.md for more
- Configure different git users to switch between
    - Using cli lib's config feature if its ever added. else can use git's config feature
- `bent ignore` to list current directory & choose a file or files to add to your gitignore. 
    - Use existing directory descendance thingy

## Low Priority Bugs
- `core_diff` breaks if `pwd !== git_root_dir`. Need to cd to the git root first or something.
- Tags are displayed in `bent switch branch`. Fix by adding `bent switch tag` and removing tags from `switch branch`

## Latest (newest to oldest)
- Recovered the scrawl extension from commit 8d4a54e
- Rename `bent_switch_default_version` to `_default_branch`
- Fixed: `bent url` failed if the remote did not have `.git` at the end of it
- Fixed: Running `bent update` will cause the `more` help menu to show, probably when it calls `run more check`.  
- Branch cleanup:
    - Make v0.4 branch. Update default branch to v0.4. Delete v0.4-candidate
    - Delete v0.3-candidate because it is was never complete and has been fully replaced by v0.4
- Disable checking for merge conflicts during `bent save`
- Add `bent reset file` command
- Converted `bent diff` to use `prompt_choose`
- Add `prompt_choose_array`, `changed_files_array_no_status` 
- Add `bent diff` to `core` group
- Write a FunctionList template in Code Scrawl
- Alternate help-menu modes 'run' & 'help'
- Write DocBlocks for the help menu
- Use CodeScrawl to parse DocBlocks & generate help menus (and api documentation)
- Write a BashGrammar in my Lexer
- cleaned up files:
    - core-unsorted.bash
    - core-update.bash
    - core.bash
    - delete.bash
    - depend.bash
    - extra.bash
    - merge.bash
    - new.bash
    - setup.bash
    - ssh-setup.bash <- fixed prompts, but other cleanup would be nice
    - ssh.bash
    - internal scripts... I think are all good
- core_merge becomes `merge` (its own group)
- convert `source_files` to use for loop

## Potential Feature Ideas
- Read the git output. & auomatically set the new remote as it suggests
```
remote: Project 'taeluf/git-project' was moved to 'taeluf/subgroup/git-project'.
remote: 
remote: Please update your Git remote:
remote: 
remote:   git remote set-url origin git@gitlab.com:taeluf/git-project.git
remote: 
remote: 
```
