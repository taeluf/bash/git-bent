#!/usr/bin/env bash

##
# Updates the cli framework to the latest version 
#
function cli_update(){
    cwd="$(pwd)"
    cd "$cli_framework_dir"
    msg_header "Update cli library at '$(pwd)'"
    bent update
    cd "$cwd"
}
