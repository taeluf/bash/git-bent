#!/usr/bin/env bash

##
#
# @tip Download all changes
function core_update(){
    is_project_dir || return;
    

    msg_status "Downloading"
    git fetch
    # msg_mistake "downloading tempoararily disabled"
    msg_status "Download complete"
    msg_header "Current status"

    branches_status

    behindCount="$(cur_branch_behind_count)"

    if [[ behindCount -gt 0 ]]; then
        msg
        msg_header "Merge..."
        msg "${cMark}$(cur_branch)${cOff} is ${behindCount} commits behind."

        changed_files_array changedFiles;
        changeCount="${#changedFiles[@]}";
        hasChanges=false
        
        if [[ $changeCount -gt 0 ]];then
            msg "${cWarn}${changeCount} local files uncommitted${cOff}."
            hasChanges=true
            if prompt_yes_or_no "Commit now?";then
                run more check
                prompt_or_quit "Commit msg" message || return
                git add .
                git commit -m "$message"
                hasChanges=false
            fi
        fi

        prompt_yes_or_no "Merge ${behindCount} commits into local ${cMark}$(cur_branch)${cOff}?" \
            || return

        git stash
        git pull
        git stash apply
        msg
    fi

    files_with_conflicts conflictingFiles
    if [[ "${#conflictingFiles[@]}" -eq 0 ]];then 
        run more check
        msg "$(cur_branch) is now up to date"
        return
    fi

    debug "run merge conflicts"  "line 57 core-more.bash"
    run merge conflicts
}


##
# Preserves your commit history & creates a new commit for the rollback
#
# @tip Rollback to a previous commit
# @todo add revert_file() to revert a single file to previous commits. Maybe show a log of # of lines changed for each previous commit
function core_revert(){
    is_project_dir || return;
    msg
    
    # @TODO Are there unsaved changes?
    # if yes, prompt
        # There are unsaved changes, what would you like to do?
        # 1. Save, then revert
        # 2. Delete unsaved changes, then revert
        # 3. Revert, then apply unsaved changes
    # if no, continue

    # <ctrl+F> [PRETTY FORMATS] on https://git-scm.com/docs/git-log
    # git log --graph --pretty=format:'%h :: %d% -- %s (%cr) ::: <%an>' --abbrev-commit -n 10
    histStr="$(git log --pretty=format:'%<(20,trunc)%h%<(30,trunc)%cr%<(30,trunc)%an%s' --abbrev-commit -n 10)"
    str_split_line "${histStr}" hist
    i=0;
    chooseCommand="prompt_choose choice '#Choose Version' \\\\\n"
    for commit in "${hist[@]}"; do

        # hash="$(sed -e "s/:::::.*//" <<< "${commit}")"
        descript="${commit:80}"
        descript="$(echo -e "${descript}" | sed -e 's/[[:space:]]*$//')"
        # msg -n "\n${descript}\n  -";

        hash="${commit:0:20}"
        hash="${hash%%*( )}"
        hash="$(echo -e "${hash}" | sed -e 's/[[:space:]]*$//')"
        # echo -n "${hash}"
        time="${commit:20:30}"
        time="$(echo -e "${time}" | sed -e 's/[[:space:]]*$//')"
        # echo -n "${time}";
        author="${commit:50:30}"
        author="$(echo -e "${author}" | sed -e 's/[[:space:]]*$//')"
        # echo -n " by ${author}";

        key="${descript}\n       - ${time} by ${author}"

        # chooseCommand+="'${key}' '${hash}' "
        # msg
        key="${key//\"/\\\"}"
        # msg "${key}"
        # continue;
        chooseCommand+="\"${hash}\" \"${hash}\" \"${key}\"  \\\\\n"

        # msg "\n\n"
        # echo "CMD::${hash}, ${key}"

        # msg
        # echo "${commit}\n"
    done
    chooseCommand+="'#This feature is experimental';"

    chooseCommand=$(msg "$chooseCommand");

    msg_status "expiremental feature"
	
    eval "$chooseCommand" \
        || return;

    # @TODO Use the hashes as keys so the commit message can be shown here as well
    header "Revert to commit ${choice}"

    git revert --no-commit "${choice}"..HEAD
    run core save

}
