#!/usr/bin/env bash

import core/core-more

# Aliases:
function core_aliases(){
    declare -n al="$1"
    al=()
    
    ## Aliases to other groups
    al+=(s:"switch")
    al+=(del:"delete")
    al+=(dep:"depend")
    # al+=(check:"check")

    ## Aliases to "more"
    al+=(url:"more url")
    al+=(check:"more check")
    al+=(status:"more check")
    al+=(ignore:"more ignore")
}

##
# Most used commands? (Idk)
#
# @order 0
# 
function core(){
    msg_instruct "try \`bent help\`"
}

## 
# Commit all files & push to origin host
#
# @tip Save your project
# @shorthand s, commit
#
function core_save(){
    is_project_dir || return;

    commitMsg="${@}"


    ##
    # disabled checking for files with conflicts because its a waste of cpu cycles & takes forever on HUGE repos
    #
    # files_with_conflicts conflictingFiles
    # if [[ "${#conflictingFiles[@]}" -gt 0 ]]; then
    #     msg_notice "Merge Conflicts in:"
    #     # msg
    #     for cf in "${conflictingFiles[@]}";do
    #         msg "  ${cf#*/}"
    #     done
    #     msg
    #     prompt_yes_or_no "Continue?" || return;
    # fi

    # @TODO Ask which changed files should be saved

    changes="$(str_trim "$(changed_files)")"

    msg_header "Author"
    msg_instruct "$(git config user.name) <$(git config user.email)>"

    if [[ "$changes" == "" ]]; then
        msg "No changes, nothing to save."
        prompt_yes_or_no "Nothing to save. Upload anyway?" na \
            || return;
        run core upload
        return;
    fi

    msg
    header "Current Status"
    msg_status "$(project_dir)"
    run more check

    msg

    changes=""
    if [[ "$commitMsg" == "" ]];then
        prompt_or_quit "Commit Msg ${cOff}or blank" changes -e \
            || return;
    else
        changes="$commitMsg"
    fi

    if [[ "$changes" == "" ]]; then
        changes="[no commit msg given]"
    fi
    git add -A
    git commit -m "${changes}"

    # @TODO integrate configs into save, upload? prompt
    # if I had a 'config' feature, I'd do 'config save_upload_answer prompt_answer', which would load the config named 'save_upload_answer' and store it in 'prompt_answer'
    # which would stop this prompt from happening
    # Possibly offer "yes, no, always, never" as options & save always/never to config
    # & `config "core save" "save_upload_answer" prompt_answer`
        # enables `bent config core save` to directly edit those config values
    prompt_yes_or_no "Upload?" \
        || return;
    run core upload
}

##
#
# @tip Upload your project (without saving first)
function core_upload(){
    is_project_dir || return;
    # @TODO only provide -u origin cur_branch when remote is not set, to avoid the message:
        # "Branch 'test-git-operations' set up to track remote branch 'test-git-operations' from 'origin'."

    run more check 

    git push -u origin "$(cur_branch)" 

    run more check 
}

##
#
# @tip Tag your project to make a packaged release
function core_tag(){
    is_project_dir || return;

    curBranch=$(project_dir)

    changed_files_array changedFiles
    if [[ "${#changedFiles[@]}" -gt 0 ]];then
        msg_notice "There are ${#changedFiles[@]} files with uncommitted changes"
    fi

    msg_header "Branch to tag"
    prompt_choose_branch branch || return

    prompt_or_quit "Tag name/version number (ex: 1.0.0)" release \
        || return;

    prompt_or_quit "Describe this tag/version: " description \
        || return

    git tag -a "$release" -m "$description" "$branch"
    git push --tags
    # git push origin :$branch
}

##
#
# @tip View changes to current files
function core_diff(){
    is_project_dir || return;
    local array_to_choose_from
    local fileToDiff
    changed_files_array_no_status array_to_choose_from 
    prompt_choose_from_array_multi=1
    prompt_choose_from_array fileToDiff 

    # echo "${fileToDiff[@]}"
    # prompt "Which file to diff?" diffFile
    git diff -- "${fileToDiff[@]}"
}
