#!/usr/bin/env bash


function delete_aliases(){
    declare -n al="$1"
    al=()
    al+=(v:"delete branc")
    al+=(version:"delete branch")
    al+=(b:"delete branch")
    # al+=(help:"delete")
    # al+=("":"delete help")
}

##
# @tip delete branches
function delete(){
    run help delete
    # run delete help "${@}"
}

##
#
# @tip Delete a branch/version of your project
delete_branch(){
    is_project_dir || return;
    
    header "Available version: "
    msg
    run show versions
    msg_instruct "[ctrl-c] to cancel"
    prompt_or_quit "Type version to delete: " delVersion \
        || return

    msg_notice "Deletes on the remote host as well"
    prompt_or_quit "Confirm version to delete: " delVersionConfirm \
        || return
    msg

    if [[ $delVersion != $delVersionConfirm ]];then
        msg "Those did not match. Try again."
        msg
        return;
    fi

    git branch -D $delVersion
    git branch -d -r origin/$delVersion
    #@TODO maybe make remote deletion optional??
    git push origin :$delVersion
    
    #@TODO provide instructions on how to restore a deleted branch
    #@TODO add archive_version to Git Bent feature (Create a tag at archive/month-year/branch-name, push, then delete the branch... or something)
}
