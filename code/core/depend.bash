#!/usr/bin/env bash

##
# experimental feature
#
function depend(){
    run help depend
}

function depend_add(){

    msg_warn "This feature is incomplete and experimental"
    prompt_or_quit "Use at your own risk" answer || return
    prompt_yes_or_no "Sure you want to continue?" || return

    prompt_or_quit "Enter git url: " gitUrl || return
    srcBranch="$(cur_branch)"
    branchName="depend_$(uniqid)"
    # debug temporary
    branchName="depend"
    repoName="$(reponame_from_url $(internet_url))"
    git checkout -b "$branchName"

    # echo "'$(cur_branch)'"
    # echo "'$branchName'"
    if [[ "$(cur_branch)" != "$branchName" ]];then
        msg_error "Failed to checkout branch"
        msg_header "Branch List"
        bent show branches
        msg_error "Failed to checkout branch"
        return
    fi

    msg
    msg_notice "We've switched to branch '$(cur_branch)' and will now delete all files to prepare for dependency loading"
    prompt_yes_or_no "Continue?" || return

    git rm -rf .
    git add -A
    git commit -m "Cleanse files for dependency loading"
    git checkout "$srcBranch"
    git merge "$branchName" -s ours --no-commit
    git add -A
    git commit  -m "Merge from depend branch"
    git checkout "$branchName"
    git pull "$gitUrl" --allow-unrelated-histories --no-commit
    git add -A
    git commit -m "git pull $gitUrl"


    # https://gitlab.com/taeluf/php/php-environment

    msg
    line_break
    msg
    msg_status "Downloaded"
    msg "We should have pulled from '$gitUrl'."
    msg_instruct "Delete any files you don't need and reorganize as you see fit."
    prompt_enter "Continue when finished"

    git add -A
    git commit -m "dependent files cleaned up"

    git tag -a "depend/$repoName" -m "Dependencies of ${gitUrl}" "$branchName"

    git checkout "$srcBranch"
    git merge "$branchName" --no-commit
    git branch -D "$branchName"

    prompt_yes_or_no "Commit now?" answer
    if [[ $answer ]];then
        git add -A
        git commit -m "dependency on ${gitUrl} merged in"
    fi
    msg
    msg_instruct "Run [bent depend update] when you need to"
    msg_instruct "[bent depend clean]"
}

function depend_update(){
    # Something like:
    # git checkout refs/tags/depend/reponame
    # git checkout -b depend
    # git pull https://gitlab.com/vendor/name --no-commit
    ## review changes
    # commit changes
    #
    # git tag -f depend/reponame 
    # bent merge depend --no-commit  ## Or I could merge the tag or merge the specific commit hash
    # git commit -m "Update dependency on ${gitUrl}"
    echo
}

function depend_remove(){
    echo
}



