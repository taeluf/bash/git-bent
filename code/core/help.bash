#!/usr/bin/env bash

function help_jargon(){
    msg_header "This help under development"

    msg_ulist  \
        "git: A type of version control, that keeps track of changes to your code. Two others are mercurial and svn." \
        "repo, repository: A project directory that is stored in version control" \
        "branch: A version of your project." \
        "There's more, but that's all I'm doing for now" \
    ;
}

# function help_cli(){
    # msg_notice "General cli help has not yet been written"
#    @TODO write general cli help
# }

# function help_bent(){
    # msg_notice "General help info about Git Bent has not been written yet"
#    @TODO write general help about Git Bent, like using q to quit prompts & how commands work & such. Maybe it could include contact info?
# }
