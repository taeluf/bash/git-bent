#!/usr/bin/env bash




##
# List commits
#
function log(){
    is_project_dir || return;
    # @TODO prompt and ask if they'd like to see file-level changes
    # @TODO Show instructions on navigating & exiting the output, then prompt (for [enter]) before continuing

    msg_instruct "git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit\n"

    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit

    msg_instruct "[bent log diff] to see more"
}

##
# List changes between commits
#
function log_diff(){
    is_project_dir || return;
    # @TODO merge into history(), via prompt...

    msg_instruct "git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit -p\n"

    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit -p
}
