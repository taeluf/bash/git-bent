#!/usr/bin/env bash

##
# 
# @tip Merge current branch into another branch
# @usage bent merge or bent merge conflicts
function merge(){
    is_project_dir || return;

    ## @TODO determine version from branch history or something
    ## @TODO add "is_saved" feature... probably put it in `bent save` to skip when already done
    ## @TODO add "pull request" approach as an option
	
    current="$(cur_branch)"
    run show versions
    msg
    msg_instruct "Experimental Feature"
    msg "  We will save any changes before integrating..."

    prompt_or_quit "Enter version to use this feature on" version \
        || return;
    # @TODO check if branch exists before continuing

    prompt_or_quit "What does this new feature do? " featureDescript \
        || return;

    msg_status "Merging feature"
    run core save
    git checkout "${version}"
    git pull
    git merge --squash "${current}"

    prompt_or_quit "Commit Changes?" commit_changes \
        || return;
    msg_status "Saving changes"
    git commit -m "${featureDescript}"
    msg_status "Uploading"
    git push

    prompt_yes_or_no "Declutter? Delete's feature branch" \
        || return;

    git branch -D "$current"
    git branch -d -r "origin/$current"
    git push origin ":$current"
}

##
# See files with merge conflicts
#
function merge_conflicts(){
    
    files_with_conflicts conflictingFiles
    msg_header "${#conflictingFiles[@]} files with merge conflicts"
    for file in "${conflictingFiles[@]}";do
        echo "  ${file#*/}"
    done

    if [[ "${#conflictingFiles[@]}" -eq 0 ]];then
        return;
    fi

    msg
    # msg "${cWarn}${#conflictingFiles[@]} files have merge conflicts${cOff}"
    prompt_yes_or_no "View instructions?" || return

    msg
    msg_header "Instructions"
    msg "Files with conflicts will look something like:"
    msg ""
    msg "<<<<<<< Updated upstream"
    msg "    //downloaded code"
    msg "    //more downloaded code"
    msg "======="
    msg "    //local code"
    msg "    //more local code"
    msg ">>>>>>> Stashed changes"
    msg ""
    msg ""
    msg " 1. Delete the code you don't want"
    msg " 2. Delete the <<<, ===, & >>> lines"
    msg " 3. Save the file to disk, like normal."
    msg " 4. [bent save] when you're done."
    msg ""

}
