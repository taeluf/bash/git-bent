#!/usr/bin/env bash

##
# Extra functions... that I'm not sure how to sort.
#
function more(){
    run help "more"
}

##
#
# @tip bent url [remote|name|ssh_key|url|new_account|default_branch|new_repo] [host_name] ... [optional arg]
function more_url(){
    msg "Use like: url [remote|name|ssh_key|url|new_account|default_branch|new_repo] [host_name]"
    url="$(url "$1" "$2")"
    msg_header "Url"
    msg "  $url"
}

##
#
# @tip Check the status of your project
function more_check(){
    is_project_dir || return;

    # msg_instruct "bent check help${cOff} for more info"
    # msg

    git status -sb
}

##
#
# @tip Download a .gitignore file from Github's gitignore repo
function more_ignore(){
    is_project_dir || return
    type="$1"

    options=()
    while read -r line; do
        name="${line##*: \"}"
        name="${name%%\",*}"
        name="${name%%\.gitignore}"
        options+=("$name")
    done < <(curl https://api.github.com/repos/github/gitignore/git/trees/master | grep --ignore-case -e ".*$type.*\.gitignore")

    msg 

    if [[ ${#options} -eq 0 ]]; then
        msg_notice "No gitignores found for '${type}'."
        msg_instruct "${cInstruct}[bent ignore]${cOff} to see a full list of options"
        msg_notice "Only 60 requests allowed per hour by github, so you may have exceeded this threshold."
        return
    fi

    prompt_args=("# Choose gitignore file")
    for opt in "${options[@]}"; do
        prompt_args+=("${opt}" "${opt,,}" "${opt}.gitignore")
    done


    
    prompt_args+=("'")
    prompt_args+=("'  File list from https://github.com/github/gitignore")
    prompt_args+=("' ${cInstruct}[bent ignore str]${cOff} to filter this list")
    prompt_choose file "${prompt_args[@]}" || return;

    file="${file}.gitignore"
    msg "Will download ${file}"
    dest="$(project_dir)/.gitignore"
    if [[ -f "$dest" ]]; then
        msg_notice "'$dest' will be overwritten!"
    fi
    prompt_yes_or_no "Download ${file} ?" || return;

    url="https://raw.githubusercontent.com/github/gitignore/master/${file}"

    content="$(curl $url)"
    curl "$url" -o "$dest"
}
