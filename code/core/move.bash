#!/usr/bin/env bash

# function move_help(){
    # msg_header "Shorthand with:"
    # msg_instruct "[bent move] runs [bent move project]"
    # n="$(color_li 0)"
    # d="$(color_li 1)"
    # f="${cOff}"
    # prompt_choose_function \
        # "# bent move [command] " \
        # "move_project" "project" "Move your project to another git host" \
    # ;
# }

##
#
# @tip Move your repo to another git host
function move(){
    # msg_instruct "[bent move help] for more"
    # move_project
    run move project
}

##
#
# @alias bent move
# @tip move your repo to another git host
function move_project(){
    is_project_dir || return

    # offer 'new_project' links
    msg_header "Start the new project"
    msg_ulist \
      "Github: $(url new_repo github)" \
      "Bitbucket: $(url new_repo bitbucket)" \
      "Gitlab: $(url new_repo gitlab)" \
    ;


    old_url=$(url url)
    old_remote=$(url remote)
    old_name=$(url name)
    old_delete=$(url delete)
    # ask for new project url
    prompt_or_quit "Paste new project url [ctrl+shift+v]: " projectUrl \
        || return
    # change 'origin' to 'old'
    git remote remove origin
    # set origin to new project url
    git remote add origin "${projectUrl}"
    # push all
    git push --mirror origin
    git remote add "$(date +%d_%m_%y_old)" "${old_remote}"

    msg
    msg_instruct "Check output for errors"
    prompt_enter "Project moved"

    # project moved message for README
    msg "\`\`\`diff"
    msg "- Project moved to $(url name)"
    msg "\`\`\`  "
    msg "See **[$(url)]($(url))**  "

    msg ""

    # Convenient links
    msg_header "Project Moved"
    msg_instruct "(hopefully) Helpful links"
    msg_olist \
      "Post 'Project Moved' msg (above): ${old_url}" \
      "Archive on old host: ${old_delete}" \
      "Check Default Branch: $(url default_branch)" \
      "View New Host: $(url name), $(url)" \
      "Old Host: $old_name, $old_url" \
    ;

}
