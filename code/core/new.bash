#!/usr/bin/env bash

function new_aliases(){
    declare -n al="$1"
    al=()
    al+=(version:"new branch")
    al+=(b:"new branch")
    al+=(p:"new repo")
    al+=(r:"new repo")
    al+=(f:"new feature")
    al+=(project:"new repo")
}


##
#
# @tip new branch/feature/repo
function new(){
    run help new
}

##
#
# @tip New branch
function new_branch(){
    is_project_dir || return

    header "Current versions for $(str_project_info)"
    run show versions
    msg;
    msg_ulist \
        "$(msg_instruct "You might use 'vbeta', 'v1', 'v2', and so on. ")" \
        "$(msg_instruct "Use only letters, numbers, hypens, periods, and underscores ")" \
    ;
    msg;
    
    prompt_or_quit "Enter new version name: " newVersion \
        || return

    git checkout -b ${newVersion}
    # @TODO should I skip the save here & JUST upload?
    prompt_yes_or_no "Upload '${newVersion}'?" \
        || return
    run core upload
}

##
#
# @tip New branch for feature development
function new_feature(){
    is_project_dir || return
    ## @TODO add show single version & use that
    ## @TODO check for conflicting branch
    header "Unpolished feature "
    msg "    This works, but needs polish & may change in the future"

    curBranch="$(cur_branch)"

    header "Your current versions are: "
    run show versions

    prompt_or_quit "Enter new feature name: " newFeature \
        || return

    branch="${curBranch}_${newFeature}"
    msg
    git checkout -b ${branch}

    msg
    msg_instruct "[bent merge] later, to integrate this feature into your project."
    msg

    prompt_yes_or_no "Upload '${branch}'?" \
        || return
    run core upload

    msg
    msg_instruct "[bent merge] later, to integrate this feature into your project."
    msg
}

##
#
# @tip Start a new repo 
function new_repo(){
	# @TODO Check url for proper pattern
	# @TODO auto-remove `git clone` from url, if that's pasted too

    dir="$(project_dir)"
    curDir="$(pwd)"
    gitUser="$(git_user)"


    if [[ $curDir == $dir ]]; then
        header "Project Exists"
        msg_olist \
            "This is already a project directory. [cd some_other_dir] then try again." \
            "Or delete your local git folder with [rm -rf .git], then try again" \
            "This Project, online: $(internet_url)" \
            "This Project, directory: ${dir}" \
        ;
        msg
        return;
    fi
    if [[ -n $dir && $dir != "" ]];then 
        msg
        msg_notice "Already Inside Project"
        msg_olist \
            "Existing Project Dir: ${dir}" \
            "New Project Dir: $(pwd)" \
            "Nested projects can be weird" \
        ;
        msg
        prompt_yes_or_no "Create Project Here? " \
            || return
    fi


    msg "";
    msg "  1. Create new project (repository) online "
    msg "     - $(url new_repo github)"
    msg "     - $(url new_repo bitbucket)"
    msg "     - $(url new_repo gitlab)"
    msg "  2. Configure the project online"
    msg "     a. A README is often good to have "
    msg "     c. You can read more about licenses at https://choosealicense.com/"
    msg "  3. Follow the prompts below"
    msg ""
    header "New Project Location:"
    msg "$(pwd)";
    msg
    prompt_or_quit "[enter] to continue" na -e \
        || return;
    msg ""
    msg "  1. Find your 'clone' ssh url."
    msg "     - Github w/ a README: Click 'Code' button then 'Use SSH' if shown. "
    msg "     - Github w/o a README: At the top, click 'SSH'."
    msg "     - Bitbucket w/o a README: Dropdown should be 'SSH'."
    msg "     - Bitbucket w/ a README: Click 'Clone' in the top-right."
    msg "  2. Copy the url that looks like ${cInstruct}git@githost.com:UserName/PROJECT-NAME.git${cOff}"
    msg "     - Do NOT copy 'git clone' if you see that before the url"
    msg ""
    
    msg_instruct "[ctrl-c] to exit"
    #@TODO auto-remove 'git clone' if it's pasted here
    prompt_or_quit "Paste your project url [ctrl+shift+v]: " projectUrl \
        || return

    msg_status "Setting up..."

    prompt_or_default "Initial Branch Name: " "main" branchName

    git init -b "${branchName}"
    git remote add origin "${projectUrl}"


    msg_status "Downloading..."

    git pull origin "${branchName}"
    git fetch

    msg_status "Saving..."

    git add -A
    git commit -m "Project Initialized from local"
    # @TODO review default branch push name
        # Will this create issues between hosts? Hub uses main. Lab uses master, I think
        # What if there's an existing branch?
    git push origin "${branchName}"

    msg_status "Done!"
    msg
    msg "Try ${cInstruct}[bent ignore]${cOff} to download a premade .gitignore file"
    msg
    #@TODO Provide some information in case there is an error here

    msg_notice "Branch '${branchName}' was pushed. Use ${cInstruct}[git push --all]${cOff} to push all other branches (if they exist)"
}
