#!/usr/bin/env bash

# Aliases:
function reset_aliases(){
    declare -n al="$1"
    al=()
    
    ## Aliases to other groups
    # al+=(s:"switch")
    # al+=(del:"delete")
    # al+=(dep:"depend")
    # al+=(check:"check")

    ## Aliases to "more"
    # al+=(url:"more url")
    # al+=(check:"more check")
    # al+=(status:"more check")
    # al+=(ignore:"more ignore")

}

##
# Reset stuff to previous states 
#
# 
function reset(){
    run help reset
}

function reset_hard(){
    msg_warn "This deletes files and all changes ON DISK to the last commit!!!"

    run core status

    prompt_yes_or_no "Continue with removing all changes listed above from the filesystem?"|| return
    msg_instruct "You can use 'git stash' to store changes locally to use later."
    prompt_yes_or_no "Are you sure you want to git reset --hard ??" || return
    git reset --hard
}

##
# Revert a file to the previous commit
#
#
function reset_file(){
    is_project_dir || return;
    local array_to_choose_from
    local fileToDiff

    msg_header "Choose a file to revert to its previous version."

    changed_files_array_no_status array_to_choose_from 
    prompt_choose_from_array fileToDiff 

    msg_instruct "File diff"
    msg

    # prompt "Which file to diff?" diffFile
    git diff -- "$fileToDiff"

    msg
    msg
    msg_instruct "Reverting $fileToDiff"
    prompt_yes_or_no "Discard these changes?" || return;

    git checkout HEAD -- "$fileToDiff"

    msg
    msg_instruct "$fileToDiff has been reverted"
}
