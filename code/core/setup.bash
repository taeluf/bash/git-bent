#!/usr/bin/env bash

import core/setup-ssh

##
#
# @tip setup things like your git user name
function setup(){
    run help "setup"
}

##
#
# @tip Configure your git environment
function setup_git(){
    header "UNPOLISHED FEATURE"
    msg "  This feature is imperect, but it should work"

    msg_instruct "[ctrl-c] to exit"
    # Configure git user
    gitUser=`git config --global user.name`
    default=""
    if [[ -n $gitUser ]]; then
        default="([blank]-use '${gitUser}')"
    fi
    prompt_or_quit "Enter git user name. ${default}:" gitUser -e \
        || return;
    if [[ $gitUser != "" ]]; then
        cleanGitUser=$(echo "${gitUser}" | sed 's/[^0-9a-zA-Z\-\_\.]//g')
        if [[ $gitUser != $cleanGitUser ]]; then 
            msg_mistake "You can only use numbers, letters, hyphens (-), and underscore (_), period (.). Run bent setup again";
            return;
        fi
        git config --global user.name $gitUser
    fi
    gitUser=`git config --global user.name`

    # Configure git email

    gitEmail=`git config --global user.email`
    default=""
    if [[ -n $gitEmail ]]; then
        default="([blank]-use '${gitEmail}')"
    fi
    prompt_or_quit "Enter public email. ${default}:" gitEmail -e \
        || return

    if [[ $gitEmail != "" ]]; then
        cleanGitEmail=$(echo ${gitEmail} | sed 's/[^0-9a-zA-Z\-\_\@\.\+]//g')
        if [[ $gitEmail != $cleanGitEmail ]]; then 
            msg "You can only use numbers, letters, hyphens (-), underscore (_), arroba (@), period (.), plus (+). Try again";
            return;
        fi
        git config --global user.email $gitEmail
    fi
    # gitEmail=`git config --global user.email`
    msg_status "Done"
}
