#!/usr/bin/env bash

function show_aliases(){
    local -n al="$1"
    al=()
    al+=("versions:show branches")
}

##
#
# @tip show... stuff
function show(){
    run help show
}

##
#
# @tip Show all versions of your project
function show_branches(){
    is_project_dir || return;
    ## @TODO check if this is a git repo & give a nice error if not
    ## @TODO make nice output for git branch -a
    git branch -a 
}


##
#
# @tip Show remotes (where this repo push/pulls from)
function show_remotes(){
    is_project_dir || return;
    git remote -v
}

##
# Show paths to all local repositories
# 
function show_local_repos(){
    #msg_notice "good"
    #find "$sourceDir/" -type d -name ".git" -print0
    find ~/ -type d -name ".git"
}

##
# Show paths to all local repositories inside current directory
#
function show_repos_here(){
    find "$(pwd)" -type d -name ".git"
}
