#!/usr/bin/env bash

# Aliases (would love to automate this, so they can be inspected @TODO)
function switch_aliases(){
    declare -n al="$1"
    al=()
    
    al+=(b:"switch branch")
    al+=(v:"switch version")
    al+=(version:"switch branch")
    al+=(p:"switch project")
    al+=(f:"switch feature")
    al+=(db:"switch default branch")
    al+=(url:"switch host url")
}

##
#
# @tip Switch branches, git hosts, & default branch
# @todo switch_feature function
function switch(){
    msg_instruct "[bent switch help] for more options"
    run switch branch "$@"
}


##
#
# @tip Switch to a different branch of your project
function switch_branch(){
    is_project_dir || return
    local branchList;

    branch_list_unique branchList refs/heads refs/remotes/origin

    curBranch="$(cur_branch)"
    
    chooseCommand=("# Choose Version")
    options=""
    for branch in "${branchList[@]}"; do
        branch=$(str_trim "${branch}")
        key="${branch}"
        branch="${branch#remote: }"
        shorthand="${branch}"
        if [[ "${branch}" == "${curBranch}" ]];then
            key="${shorthand} (current)"
        fi
        chooseCommand+=("${branch}" "${shorthand}" "${key}")
    done;

    prompt_answer="$1"
    prompt_choose choice "${chooseCommand[@]}"
    # eval $chooseCommand

    prompt_exited "$choice" && return;

    git checkout "${choice}"

    return;
}

##
#
# @tip Change your git host
function switch_project(){
    # @TODO implement this feature
    
    msg_header "Switch between projects"
    msg
    msg_mistake "This feature is not yet available. But you can:"
    msg_instruct "[cd /path/to/your/project]"
    msg "Then run [bent ...] to work with that project"

}

##
#
# @tip Go online to change the default branch of your project
function switch_default_branch(){
    is_project_dir || return;

    header "We'll guide you through it. (instructions best suited for github)"
    
    ## @TODO learn about the problems this can cause & notify the user of them.
    ## @TODO Rename this function
    ## @TODO add good instructions for each git host

    url="$(url default_branch)"
    
    msg "  0. Versions are called 'branches' on git"
    msg "  1. Go to ${url}"
    msg "  2. Click the dropdown under 'Default Branch'"
    msg "  3. Choose the version (branch) you want as your default"
    msg "  5. Click 'Update' button & confirm the popup. "

    msg
}


##
#
# @tip Change the git host this repo push/pulls from
function switch_host(){

    # @TODO I don't know... Just make this better. More guided. Clearer information?
    # @TODO use prompt_exited
    # @TODO present remote name & url for confirmation before setting 

    header "Your current urls are: "
    git remote -v
    msg ""
    msg "1. You will usually change 'origin' "
    msg "2. If there are others, you might need to update them as well."
    msg ""
    msg_instruct "[ctrl-c] to exit."
    prompt_or_quit "Which one will you change? [type it]: " remote \
        || return
    prompt_or_quit "Enter new URL: " url \
        || return
    msg ""
    git remote set-url ${remote} ${url}
    git remote set-url --push ${remote} ${url}
    msg_status "Host url updated"
    #@TODO offer upload after changing url

}
