prompt_choose_function "# show [command]${cOff}-"  \
"${help_mode} aliases" "aliases" "-- no description" \
"${help_mode} " "-default-" "-- no description" \
"${help_mode} branches" "branches" "-- no description" \
"${help_mode} remotes" "remotes" "-- no description" \
"${help_mode} local repos" "local repos" "Show paths to all local repositories" \
"${help_mode} repos here" "repos here" "Show paths to all local repositories inside current directory"