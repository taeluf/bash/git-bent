prompt_choose_function "# ssh [command]${cOff}-"  \
"${help_mode} keygen" "keygen" "Generate a new ssh key to securely sign into git without a password" \
"${help_mode} " "-default-" "-- no description" \
"${help_mode} which" "which" "Configure which ssh private key file to use" \
"${help_mode} add" "add" "Add an ssh key to authentication agent"