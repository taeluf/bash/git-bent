#!/usr/bin/env bash

is_project_dir(){
    local dir
    dir=$(project_dir)
    if [[ $dir != "" ]];then
        return 0;
    fi
    echo ""
    msg_mistake "$(pwd) is not a project directory. Try again. "
    echo ""
    return 1;
}

project_dir(){
    local dir;
    dir=$(git rev-parse --show-toplevel 2>/dev/null)
    echo $dir;
}

## Example:
# is_outside_project && msg_mistake "You're not currently in a project." && msg_instruct "Use [bent switch project]" && return;
is_outside_project(){
    dir=$(project_dir)
    if [[ $dir != "" ]];then
        return 1;
    fi
    return 0;
}



changed_files(){
    # @TODO use porcelain command?
    local files;
    files="$(git status -su)"
    msg "$files";
}
##
# Get an array of files that have changed since last commit
# @arg outputArray
changed_files_array(){
    local -n files=${1}
    modType=${2:-"all"}
    files=()

## Sample output from status
#  M .vscode/settings.json
#  D LICENSE
# AM cats
# D  code/help.sh
# A  f/1
# ?? blah.3
# ?? code/help.sh
# ?? dont-touch-cats-lol
# ?? f/2
    status="$(git status -su --porcelain=v1)"
    if [[ "$status" == "" ]];then
        return
    fi
    str_split_line "$status" files

}
changed_files_array_no_status(){
    local -n files=${1}
    modType=${2:-"all"}
    files=()

## Sample output from status
#  M .vscode/settings.json
#  D LICENSE
# AM cats
# D  code/help.sh
# A  f/1
# ?? blah.3
# ?? code/help.sh
# ?? dont-touch-cats-lol
# ?? f/2
    status="$(git status -su --porcelain=v1)"
    if [[ "$status" == "" ]];then
        return
    fi
    str_split_line "$status" statusyFiles
    for f in "${statusyFiles[@]}"; do
        f="${f# }"
        f="${f:1}"
        while [[ "${f:0:1}" != " " ]];do
            f="${f:1}"
        done
        f="${f# }"

        files+=("$f")
    done

}

conflicting_files(){
    local files;
    files="$(git diff --name-only --diff-filter=U)"
    msg "$files"
}

merge_conflicts(){
    files_with_conflicts output_files
    for f in "${output_files[@]}";do
    echo "${f} has a merge conflict"
    done
}

files_with_conflicts(){
    local -n outputFiles="$1"
    # local files fileShort dir bn c1 c2
    # local files
    # local file
    local i
    outputFiles=()
    # Any files with merge conflicts have <<<, ===, & >>>
    # <<<<<<<   =======  >>>>>>>i
    # msg
    # msg
# <<<<<<<
    dir="$(project_dir)"
    str_split_line "$(grep -r "^<<<<<<< " "${dir}")" files
    dir="$(project_dir)"
    bn="$(basename "${dir}")"
    # echo "--"
    # echo "${files[@]}"
    # echo "++"
    # return
    for i in "${files[@]}"; do 
        # echo "i:${i}"
        # echo "///"
        conflict_file=$(sed -e "s/:<<<<<<< .*//" <<< "${i}")
        if [[ ! -f "$conflict_file" ]];then continue; fi
        fileShort="${conflict_file#"${dir}"}"
        fileShort="${bn}${fileShort}"

        c1=$(grep -c "^=======" "${conflict_file}")
        c2=$(grep -c "^>>>>>>> " "${conflict_file}")
        if [[ c1 -gt 0 && c2 -gt 0 ]]; then
            # echo "${fileShort} has a merge conflict"
            # echo "${fileShort}"
            outputFiles+=("${fileShort}")
        fi
    done

    # echo "zeep zeep"
    # echo "${outputFiles[@]}"
    # echo "zorb zorb"
}

