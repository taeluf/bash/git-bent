#!/usr/bin/env bash

setup_guided(){
    cli_tips
    setup
    how_to_setup_git
    check_git_install
    configure_git
    ssh_setup
}

cli_tips(){
    echo ""
    echo "Some Tips";
    echo "  1. For any prompts, press [enter] to confirm your answer."
    echo "  2. You may see things like (y-yes/n-no/[blank]-use default). "
    echo "     This means: "
    echo "     a. Type \"y\" for yes"
    echo "     b. Type \"n\" for no"
    echo "     c. Leave blank & press [enter] to use the default";
    echo "  3. Press [ctrl+c] any time to cancel this script."
    echo "  4. [ctrl+click] to open links in your browser"
    echo ""
    
    prompt "[enter] to continue"
}
how_to_setup_git(){
	# @TODO Add mention of GitLab & Gittea. Maybe change the information I share?
	# @TODO make this accessible directly (instead of just through bent setup)
    echo ""
    echo "Git hosting:"
    echo "  1. Create a hosted git account"
    echo "     a. Gitlab @ $(url new_account gitlab) owned by Gitlab, Inc.; hosted by Cloudflare"
    echo "     b. Bitbucket @ $(url new_account bitbucket) owned by Atlassian; hosted by Amazon"
    echo "     c. Github @ $(url new_account github) owned by Microsoft; hosted by Amazon"
    echo "  2. Setup an account & choose a username."
    echo ""

}

check_git_install(){
    # Check if git is installed
    if ! command -v "git" &> /dev/null
    then
        msg "\n----git needs to be installed----\n"
        echo "Go to https://git-scm.com/downloads"
        echo "    (ctrl+click to open link)"
        echo "run \`bent setup\` again once git is installed"
        return
    fi

    msg_status "Git is installed"
}

str_project_info(){
    local info;
    if [[ ! is_outside_project ]]; then 
        info="not a project";
        echo "$info";
        return;
    fi

    dir=$(git rev-parse --show-toplevel 2>/dev/null)
    base=$(basename "$dir");
    info="${base}";

    echo "$info";
}


git_user(){
    local user;
    user=$(git config --global user.name)
    echo $user;
}


