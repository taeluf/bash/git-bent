#!/usr/bin/env bash

function archive(){
    echo "archive not implemented";
    return;
    echo "Will Archive branch at $(git rev-parse --show-toplevel)."
    echo "You must not be on the branch you're archiving."
    git config --get remote.origin.url
    git branch -a
    read -p "Enter Branch Name: " branch;
    # git checkout master
    case $branch in
        "" ) echo "Empty branch name. Exiting";exit;;
        "q" ) echo "Empty branch name. Exiting";exit;;
        "n" ) echo "Empty branch name. Exiting";exit;;
    esac

    git tag archive/$branch $branch
    git push --tags
    git branch -D $branch
    git branch -d -r origin/$branch
    git push origin :$branch
}
