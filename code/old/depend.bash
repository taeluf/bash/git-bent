#!/usr/bin/env bash

function depend(){
    is_project_dir || return

    # echo "Bent depend not currently available"
    # return;

    dependConfigFile="${projectDir}/.bent/depend.bash"
    if [[ -f "${dependConfigFile}" ]] && prompt_yes_or_no "Load config file?" ;then
        ## offer update
        ## Ask about adding a new dependency
        echo "You have a config file"
    fi

    url="${1}"
    prompt_answer="$2"

    download_from_git dependDir "$url" "$prompt_answer"

    fileList=()
    dirList=()
    choose_files_recursively "$dependDir"
    srcFileList=("${fileList[@]}")

    i=0
    srcFiles=()
    destFiles=()
    projectDir="$(project_dir)"

    if [[ "${#srcFileList[@]}" -le 0 ]];then
        msg_notice "No files selected to copy"
        return
    fi

    msg_header "Copy Files?"
    depend_config="url=\"${url}\""

    msg_instruct "Enter relative destination path (s-skip)"
    for srcFile in "${srcFileList[@]}";do
        srcRel="${srcFile##"${dependDir}"}"
        srcRel="${srcRel##/}"
        prompt "${cOff}${srcRel} ${cCommand}->${cOff} " destRel
        if [[ "$destRel" == "s" ]];then continue;fi;
        # if prompt_exited "$destRel"; then continue; fi;
        destFile="${projectDir}/${destRel}"
        destRel="${destRel##/}"
        # msg "$srcRel -> $destRel"
        depend_config+="$(msg "\nsrcFiles+=(\"${srcRel}\")")"
        depend_config+="$(msg "\ndestFiles+=(\"${destRel}\")")"

        srcFiles+=("${srcFile}")
        destFiles+=("${destFile}")
    done

    prompt_yes_or_no "Continue with copy?" || return;


    if [[ ! -d "${projectDir}/.bent" ]];then
        mkdir "${projectDir}/.bent"
    fi
    echo "${depend_config}" > "${dependConfigFile}"

    for ((i=0;i<${#destFiles[@]};i++));do
        cp -r "${srcFiles[$i]}" "${destFiles[$i]}"
    done

    # echo "${depend_config[@]}"

    

    # echo "${destFiles[@]}"
    # echo "${srcFiles[@]}"
    # echo "++"

    # echo "${answer[@]}"

    # msg -n "cd back to project dir"
    # popd
    # echo 'zeeb'
    return

    
    prompt "Enter relative file path to use:" relPath

    fPath="${downloadDir}/${relPath}"
    if [[ ! -f "$fPath" ]];then
        echo 'not a file'
        return;
    fi

    prompt "Enter destination relative path:" destRel
    curDir="$(project_dir)"

    destPath="${curDir}/${destRel}"
    if [[ -f "$destRel" ]];then
        prompt_yes_or_no "This will overwrite $destRel" || return
    fi

    cp -i "$fPath" "$destPath"
}

download_from_git(){
    local -n downloadDir="$1"
    url="$2"
    prompt_answer="$3"
    clean="${url%%\.git}"
    clean="${clean%%\/}"
    repoName="${clean##*/}"

    downloadDir=~/.cache/"${repoName}"
    if [[ ! -d "$(dirname "$downloadDir")" ]];then
        mkdir "$(dirname "$downloadDir")"
    fi

    if [[ -d "$downloadDir" && -d "$downloadDir/.git" ]]; then
        msg "Files already downloaded"
        prompt_yes_or_no "Update repo?" || return
        echo "$(cd "$downloadDir";git pull;)"
    else
        echo "$(cd "$downloadDir";git clone --single-branch --no-tags "$url" "$downloadDir")"
    fi

    if [[ ! -d "$downloadDir" || ! -d "$downloadDir/.git" ]]; then
        msg_notice "Something went wrong with download"
    fi
}
