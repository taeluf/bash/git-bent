#!/bin/bash

help_menu(){
    do_help $1 0 "this will be removed from help menu" || return;
    prompt_choose_command \
        "# How can I help?" \
            "help_intro" "Introduction" \
            "help_setup" "Setup your environment" \
            "help_basics" "Basic commands (new, save, switch, upload, update)" \
            "help_basics_plus" "More commands (status, release, history, rollback)" \
            "help_project" "Project Maintenance " \
            "help_all" "All commands" \
    ;
}



help_intro(){
    prompt_choose_command \
        "help_intro_usage" "Tips & Usage" \
        "help_intro_vcs" "What is git?" \
        "help_intro_this" "What is Git Bent?" \
        "help_intro_more" "How can I get more help?" \
    ;
}

help_setup(){
    n="$(color_li 1)"
    d="$(color_li 0)"
    f="${cOff}"
 #       "setup" "${n}[bent setup]$f: Setup your GIT environment & get started online" \
    prompt_choose_command \
    	"how_to_setup_git" "Sign up for a git host" \
        "configure_git" "${d}[bent configure git]$f: Set your git username & public email address" \
        "ssh_setup" "${n}[bent ssh setup]$f: Setup a security token for better security & passwordless login" \
        "load_ssh" "${d}[bent load_ssh]$f: Initialize ssh keys so you can upload/download files without a password" \
        "check_git_install" "Check if git is installed" \
    ;
}

help_basics(){
    msg_header "Choose one!"
    msg_instruct "Skip this menu by typing the command shown..."
    n="$(color_li 1)"
    d="$(color_li 0)"
    f="${cOff}"
    prompt_choose_command \
        "new" "${n}[bent new]$f: Start a new project, version, or feature" \
        "save" "${d}[bent save]$f: Save your project with a description" \
        "switch" "${n}[bent switch]$f: Switch versions, features, or projects" \
        "upload" "${d}[bent upload]$f: Upload to your git host" \
        "update" "${n}[bent update]$f: Download any changes from your git host" \
    ;
    # Maybe add 'use'
}

help_basics_plus(){
    n="$(color_li 1)"
    d="$(color_li 0)"
    f="${cOff}"
    prompt_choose_command \
        "status" "${n}[bent status]$f: See the current status of your project" \
        "release" "${d}[bent release]$f: Upload a numbered release of your project" \
        "history" "${n}[bent history]$f: See a history of your saves" \
        "history_changes" "${d}[bent history changes]$f: See what changed in your files" \
        "rollback" "${n}[bent rollback]$f: Revert to a previous save of your project" \
        "use_feature" "${d}[bent use feature]$f: Use your current feature in another version" \
    ;
}
help_project(){
    n="$(color_li 1)"
    d="$(color_li 0)"
    f="${cOff}"
    prompt_choose_command \
        "default_version" "${n}[bent default version]$f: Change your project's default version" \
        "show_url" "${d}[bent show url]$f: Show the online URL for your project" \
        "change_url" "${n}[bent change url]$f: Change the url your project is uploaded to" \
        "show_versions" "${d}[bent show versions]$f: Show the versions of your project" \
        "delete_version" "${n}[bent delete version]$f: Delete a version of your project" \
    ;
    # Maybe add 'use'
}




help_intro_usage(){
    msg_header "Usage & Tips"
    msg_ulist \
    	"Type [bent] for help" \
        "Press [ctrl+c] any time to cancel anything " \
        "When prompted:\n    - type [y] or [a] for yes/accept\n    - type [c] or [q] or [n] for cancel / quit / no.\n      - It's always your choice" \
        "Type [bent] followed by a command to do a thing. \n    - Ex: [bent save]" \
        "Don't ever type the [brackets], just the thing inside" \
        "[up] & [down] arrows: Cycle through commands you've used" \
        "[ctrl+click] links to open them in your browser (in most terminals)" \
        "[ctrl+shift+c] & [ctrl+shift+v] to copy & paste (in most terminals)" \
    ;
}

help_intro_vcs(){
    msg_header "About Git & Version Control"
    msg "See https://www.git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F for a lot of details."
    msg "Basically, git is a way to handle different versions of your project."
    msg "It keeps track of changes you make and allows you to revert to old versions."
    msg "Or work on different features without messing up the central code base."
    msg "We should probably tell you more... but haven't gotten around to it yet."
}

help_intro_this(){
    msg_header "About Git Bent"
    msg "Git Bent is a user-friendly wrapper of git. You can always use git directly if need be."
    msg "We hope getting started with Git Bent feels okay for newcomers."
    msg "And for more experienced folk, we hope to save you many, many keystrokes, as well as some internetting."
}

help_intro_more(){
    msg_header "Get help with Git Bent"
    msg_instruct "[ctrl+click] to open links"
    msg
    msg "See https://github.com/Taeluf/Git-Bent/issues or https://twitter.com/TaelufDev"
}


