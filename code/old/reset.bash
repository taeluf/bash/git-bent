#!/usr/bin/env bash

reset(){
    echo "reset not implemented";
    return;
    # @TODO integrate any of the useful code from here with update(). Then delete this file.

    git fetch
    line_break
    changedFiles="$(changed_files)"
    branch="$(cur_branch)"

    if [[ $changedFiles != "" ]]; then
        msg ""
        header "Some Files are different from the git host"
        echo ""
        msg "$changedFiles"
        echo ""
        
        echo "  - M means 'modified' since last save."
        echo "  - ?? means 'untracked'. It hasn't been saved before."
        # echo "    - It will NOT be overwritten"
        echo "  - D means 'deleted' since last save."
        echo "  - A means 'added' or file created since last save."
        echo ""

        # msg -n "${cCommand}"
        # prompt "$(msg -n "${cCommand}[enter] to see your options${cOff}")"
        prompt "[enter] to see your options"
        # prompt "[enter] to see your options"
        
        echo ""
        echo "  1. Overwrite all local files."
        echo "     - ?? files will be deleted"
        echo "  2. Overwrite tracked local files"
        echo "     - ?? files will be kept"
        # echo "     - We'll back them up first."
        echo "  2. Resolve conflicts by hand"
        echo "     - This is more advanced. We'll try to help you."
        echo "  3. Cancel & save your changes."
        echo ""
        echo " - Type the option you choose"
        prompt "(overwrite-all / overwrite-tracked / resolve / cancel): " option

        if [[ $option == "overwrite-tracked" ]]; then 
            # git fetch
            git reset --hard "origin/${branch}"
        elif [[ $option == "overwrite-all" ]]; then
            git add -A
            # git fetch
            git reset --hard "origin/${branch}"
        elif [[ $option == "resolve" ]]; then
            # git fetch
            git stash
            git reset --hard "origin/${branch}"
            changedFiles="$(changed_files)"
            if [[ $changedFiles != "" ]]; then
                echo ""
                echo "##################################"
                echo ""
                echo "You currently have what's on the git host, plus the following untracked files: "
                echo ""
                msg "${changedFiles}"
                echo ""
                validResponse= false
                while [[ ! $validResponse ]]; do
                    prompt "Keep these files? ([type] yes / no): " keepFiles
                    if [[ $keepFiles == "no" ]]; then
                        git add -A
                        git reset --hard "origin/${branch}"
                    elif [[ $keepFiles == "yes" ]]; then
                        # git add -A
                        # git commit -m "Saving untracked files during bent reset, before resolving conflicts by hand"
                        # git push origin
                        break;
                    else 
                        echo "Your response was not understood. Please answer 'yes' or 'no'"
                        continue;
                    fi
                    validResponse=true;
                done
            fi
            echo ""
            echo "...Restoring your local changes."
            applyOutput="$(git stash apply)"
            echo ""
            echo ""
            conflicts="$(conflicting_files)"
            echo "The following files have conflicts: "
            echo ""
            msg "${conflicts}"
            echo ""
            prompt "[enter] for instructions ";
            echo ""
            echo "Now open each file in your IDE or text editor (one at a time)."
            echo "You should see something like:"
            echo ""
            echo "<<<<<<< Updated upstream"
            echo "    //code from git host"
            echo "    //more code from git host"
            echo "======="
            echo "    //unsaved local code"
            echo "    //more local code"
            echo ">>>>>>> Stashed changes"
            echo ""
            echo ""
            echo " 1. Delete the <<<, ===, & >>> lines"
            echo " 2. Delete the code you don't want"
            echo " 3. Save the file to disk, like normal."
            echo " 4. Proceed below"
            echo ""
            prompt "[enter] to continue "
            echo ""
            # prompt "Describe the changes you've made: " description
            save
        fi

        return;
        
    fi

    echo "yes";
    # echo "${changedFiles}";

    return;

    echo " -- This will delete any changes you've made since .... whatever";
    echo ""
    prompt "Reset project to match the git host? (y-yes/n-no)" resetProject
    if [[ $resetProject != "y" && $resetProject != "Y" ]];then
        return;
    fi
    branch="$(cur_branch)"

    git fetch
    git reset --hard "origin/${branch}"
}
