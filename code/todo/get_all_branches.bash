#!/usr/bin/env bash
##
# Download all branches for a git repo into a current_dir/$repo_name/[branch_name]
# Downloads the git repo into ...$repo_name/.git/
#
# @param $1 $repo_url the git url for cloning the repo
# @param $2 $repo_name the local name of the repo & the dir it will be cloned into

current_pwd="$(pwd)"

repo_url="$1"
repo_name="$2"

echo ""
echo "Will download $repo_name into $(pwd)/$repo_name"

echo ""
read -p "Continue?(y/n) " answer
echo "$answer"

if [[ ! $answer  =~ "y" ]];then
    echo "exit"
    echo ""
    exit;
fi


git clone --mirror "$repo_url" "$repo_name/.git"
cd "$repo_name"/.git

echo "$(pwd)"

for branch in $(git for-each-ref --shell --format="%(refname)"  --sort='-authordate:iso8601' --sort='refname:rstrip=-2' ); do
    branch="${branch#\'}"
    branch="${branch%\'}"
    name="${branch#refs/heads/}"
    if [[ "${name}" == "${branch}" ]];then
        ## means its probably a tag ? ... maybe i should do tags, too ... idc rn
        continue;
    fi
    git worktree add ../"$name" "$name"
done

cd "$current_pwd"
