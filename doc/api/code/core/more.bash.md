# File code/core/more.bash
## Functions
- `more`: Extra functions... that I'm not sure how to sort.
- `more_url`: bent url [remote|name|ssh_key|url|new_account|default_branch|new_repo] [host_name] ... [optional arg]
- `more_check`: Check the status of your project
- `more_ignore`: Download a .gitignore file from Github's gitignore repo
