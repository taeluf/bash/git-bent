# File code/core/show.bash
## Functions
- `show_aliases`: 
- `show`: show... stuff
- `show_branches`: Show all versions of your project
- `show_remotes`: Show remotes (where this repo push/pulls from)
- `show_local_repos`: Show paths to all local repositories
- `show_repos_here`: Show paths to all local repositories inside current directory
