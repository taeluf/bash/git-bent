# File code/core/switch.bash
## Functions
- `switch_aliases`: 
- `switch`: Switch branches, git hosts, & default branch
- `switch_branch`: Switch to a different branch of your project
- `switch_project`: Change your git host
- `switch_default_branch`: Go online to change the default branch of your project
- `switch_host`: Change the git host this repo push/pulls from
