# File code/lib/files.bash
## Functions
- `is_project_dir`: 
- `project_dir`: 
- `is_outside_project`: Example:
is_outside_project && msg_mistake "You're not currently in a project." && msg_instruct "Use [bent switch project]" && return;
- `changed_files`: 
- `changed_files_array`: Get an array of files that have changed since last commit
- `changed_files_array_no_status`: Sample output from status
 M .vscode/settings.json
 D LICENSE
AM cats
D  code/help.sh
A  f/1
?? blah.3
?? code/help.sh
?? dont-touch-cats-lol
?? f/2
- `conflicting_files`: Sample output from status
 M .vscode/settings.json
 D LICENSE
AM cats
D  code/help.sh
A  f/1
?? blah.3
?? code/help.sh
?? dont-touch-cats-lol
?? f/2
- `merge_conflicts`: 
- `files_with_conflicts`: 
